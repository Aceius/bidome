const main = () => {
    let w = document.createTreeWalker(document.body, NodeFilter.SHOW_TEXT, null, false); 
    let n;

    while (n = w.nextNode()) {
        n.nodeValue = n.nodeValue.replace(/(joe )?biden/ig, 'Jo Bidome');
    }
};

setInterval(main, 100);